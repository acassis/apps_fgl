#include "display.h"
 
void *screen;

int xres = 240;
int yres = 320;

#define NULL ( (void*) 0 )

fgl_texture * _display_buffer = NULL;

void display_buffer_set(fgl_texture* inTarget) {
	_display_buffer = inTarget;
}

fgl_texture* display_buffer_get() {
	return _display_buffer;
}



fgl_texture* display_init() {
	/*if(!glfwInit())
		return NULL;

	glfwOpenWindowHint(GLFW_WINDOW_NO_RESIZE, GL_TRUE);
	if(!glfwOpenWindow(320, 240, 5, 6, 5, 0, 0, 0, GLFW_WINDOW)) {
		glfwTerminate();
		return NULL;
	}
	glfwSetWindowTitle("display");

	if(glewInit() != GLEW_OK) {
		glfwTerminate();
		return NULL;
	}

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	int tempViewport[4];
	glGetIntegerv(GL_VIEWPORT, tempViewport);
	glOrtho(0, tempViewport[2], tempViewport[3], 0, -1, 1);
	if(glGetError() != GL_NO_ERROR) {
		//glfwTerminate();
		return false;
	}*/

	//screen = SDL_SetVideoMode(xres, yres, 16, SDL_SWSURFACE);
	//if (screen == NULL) {
	//	fprintf(stderr, "Unable to set video mode: %s\n", SDL_GetError());
	//	return NULL;
	//}

	_display_buffer = (fgl_texture *) fgl_texture_create(xres, yres);
	if(_display_buffer == NULL) {
		//glfwTerminate();
		return NULL;
	}
	fgl_texture_clear(_display_buffer);
	return _display_buffer;
}

void display_term(){
	//glfwTerminate();
	fgl_texture_delete(_display_buffer);
	_display_buffer = NULL;
}



void display_clear() {
	fgl_texture_clear(_display_buffer);
}

fgl_texture* display_flip(void *fbmem) {
	memcpy(fbmem, _display_buffer->data, xres*yres*2);
}

